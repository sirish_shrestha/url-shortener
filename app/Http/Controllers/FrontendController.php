<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Services\FrontendAPIService;


class FrontendController extends Controller
{
	protected $request;
	protected $FrontendAPIService;

	public function __construct(Request $request, FrontendAPIService $FrontendAPIService){
		$this->request = $request;
		$this->FrontendAPIService = $FrontendAPIService;
	}
    /*
    Function to show homepage
    */
    public function index(){
    	return view('frontend.home');
    }
    
    /*
    Function to call the createShortURL api
    */
    public function processURL(){
    	return $this->FrontendAPIService->CallCreateShortUrlAPIAndRedirect();
	}

    /*
     * Function to redirect to long URL from Short URLs
    */
	public function redirect($url_hash){
		return $this->FrontendAPIService->checkAndRedirectToLongUrl($url_hash);
	}
}
