<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class URLController extends Controller
{
   protected $accessToken;
   public function __construct()
    {
        $this->middleware('auth');
    }
    /*
    Function to list the URLs in the Admin Panel
    */
    public function index()
    {
        $user = \Auth::user();
        return view('urls.list')->with('accessToken', $user->api_token);
    }
}
