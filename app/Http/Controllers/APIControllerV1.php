<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Services\APIService;
use Illuminate\Support\Facades\Input;

class APIControllerV1 extends Controller
{
    protected $request;
    protected $APIService;

    public function __construct(Request $request, APIService $APIService){
    	$this->request = $request;
        $this->APIService = $APIService;
    }


    /*
    Function to return the list of short URLs in JSON format to be fed to Data Tables.
    */
    public function getUrlJSON(){
    	return $this->APIService->getUrlListForDataTables();
    }
	
	/*
    Function to get Short URL mapping
    */
    public Function getShortUrl($id){
    	return $this->APIService->getShortUrl($id);
    }

	/*
    Function to delete Short URL mapping
    */
    public Function deleteShortUrl($id){
    	return $this->APIService->deleteShortUrl($id);
    }

    /*
    Function to create the SHORT URL
    */
    public function createShortURL(){
        $long_url = $this->request->input('long_url');
        $expires_on = $this->request->input('expires_on');
        $rules = array(
            'long_url' => 'required|url',
            'expires_on' => 'nullable|date_format:Y-m-d|after:today' );
        //Run the form validation
        $validation = Validator::make(Input::all(),$rules);
        if($validation->fails()) {
            $msgReturn = ['status' => 'validationError', 'message' => $validation->errors()];
            return $msgReturn;
        }
        return $this->APIService->createShortURL();
    }
}