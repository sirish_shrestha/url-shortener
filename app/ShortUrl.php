<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortUrl extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    
    /*
	Function to increase hit counter of the broswed URL
    */
    public function incrementHitCounter(){
    	$this->hits = $this->hits +1;
    	return $this;
    }
}
