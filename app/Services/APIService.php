<?php 

/**
 * API Services for ADMIN Interface
 *
 * @author Sirish Shrestha
 */
namespace app\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\ShortUrl;
use Carbon\Carbon;

class APIService{
	protected $request;
	
	public function __construct(Request $request) {
        $this->request = $request;
    }

    /*
    Get urls list for displaying in data tables
    */
    Public function getUrlListForDataTables(){
    	#get order params
        $order = $this->request->input('order');
        $sidx = $order[0]['column'];
        $sord = $order[0]['dir'];
        #get pager params
        $draw = $this->request->input('draw');
        $start = $this->request->input('start');
        $limit = $this->request->input('length');
        $searchTextLong = $this->request->input('long_url_search');
        $searchTextShort = $this->request->input('short_url_search');

        $page = $start / $limit;
        #now hit it
        $shortUrls = new ShortUrl();
        #columns to draw / search
        $gridColumns = ['id', 'long_url', 'url_hash', 'hits', 'expires_on'];
        #get column name for which sorting to be done, if avail
        $sidx = $gridColumns[$sidx];
        if (strlen($sidx) == 0) {
            $sidx = 'id';
            $sord = 'DESC';
        }
        #no of records to skip to get to start item of prescribed page
        $skip = $page * $limit;

        //Conditions for Search Texts if any
        $conditionSearch = array();
        array_push($conditionSearch,['is_deleted','=',0]);
        if (strlen($searchTextLong) > 0) {
        	$searchTextLong = '%' . $searchTextLong . '%';
            array_push($conditionSearch,['long_url', 'LIKE', $searchTextLong]);
        }
        if (strlen($searchTextShort) > 0) {
        	$searchTextShort = '%' . $searchTextShort . '%';
            array_push($conditionSearch,['url_hash', 'LIKE', $searchTextShort]);
        }

        
        $shortUrlList = $shortUrls->select('id','long_url','url_hash','hits','expires_on')
        	->where($conditionSearch);
        //Get the count first
        $totalUrls = $shortUrlList->get()->count();
       
        #patch order by with limit and get the array
        $shortUrlList = $shortUrlList->orderBy($sidx, $sord)->skip($skip)->take($limit)->get($gridColumns)->toArray();
        #datatables requires NUM_ARRAY so manually traverse through the array from recordset to grab num array
        if (sizeof($shortUrlList) > 0) {
            foreach ($shortUrlList as $k => $url) {
                $shortUrlData[$k] = array_values($url);
                $real_id = $shortUrlData[$k][0];
                $shortUrlData[$k][0] = $k+1;
                $shortURL =  "http://short.ly/". $shortUrlData[$k][2];
                $shortUrlData[$k][2]  = $shortURL."&nbsp;<a href='".$shortURL."' target='_blank'><i class='fa fa-link'></a></i>";
                $shortUrlData[$k][4]  = is_null($shortUrlData[$k][4])?"N/A":Carbon::parse($shortUrlData[$k][4])->format("M j, Y g:iA");
                //add real id for identification
                array_push($shortUrlData[$k], $real_id);
                #empty placeholder for actions!
                array_push($shortUrlData[$k], '');
            }
        } else {
            $shortUrlData = [];
        }
        #format in which datatable expects table data
        return [
            'draw' => $draw,
            'limit' => $limit,
            'recordsTotal' => $totalUrls,
            'recordsFiltered' => $totalUrls,
            'data' => $shortUrlData];

    }
    /*
    Function to get the Short URL mappings
    */
    public function getShortUrl($id){
    	$shortURL = ShortUrl::find($id);
    	return $shortURL;
    }
    
    /*
    Function to delete the Short URL mappings
    */
    public function deleteShortUrl($id){
    	//$shortURL = ShortUrl::findOrFail($id);
    	//$shortURL->update(['is_deleted',1]);
    	$shortURL = ShortUrl::find($id);

		if($shortURL) {
		    $shortURL->is_deleted = 1;
		    $shortURL->save();
		    return ['status' => TRUE];
		}else{
           return ['status' => FALSE]; 
        }
    }

    /*
    Function to validate and create Short URLs from the provided long URL
    */
    public function createShortURL(){
        $long_url = urldecode(trim($this->request->input('long_url')));
        $expires_on = trim($this->request->input('expires_on'));
        if($expires_on == ""){
            $expires_on = null;
        }
        //If the url is same as current domain do not shorten it.
        if($long_url == "http://short.ly"){
            $msgReturn = ['status' => 'error', 'message' => 'Invalid URL. Please try another URL.'];
            return $msgReturn; 
        }
        //IF the url already exists in the database, return the same short URL
        $url = ShortUrl::where('long_url', '=' ,$long_url)->first();
        if($url !== null){
            $shortURL = url('/').'/'.$url->url_hash;
            $hits = $url->hits;
            $expires_on = $url->expires_on;

            $msgReturn = ['status' => 'success', 'message' => 'URL exists in our database!', 'short_url' => $shortURL, 'hits' => $hits, 'expires_on' => $expires_on, 'long_url' => $long_url];
            return $msgReturn;     
        }
        //CREATE SHORTENED URL AND SAVE TO DATABASE
        $url = ShortUrl::select('url_hash', 'long_url')->orderBy('created_at','DESC')->orderBy('url_hash','DESC')->limit(1)->first();
        $latestHash =  $url->url_hash;
        $newHash = get_next_alphanumeric($latestHash);
        $shortURL = url('/').'/'.$newHash;
        $short = new ShortUrl;
        $short->long_url =  $long_url;
        $short->expires_on =  $expires_on;
        $short->url_hash =  $newHash;
        $short->hits =  0;
        $short->is_deleted =  0;
        $short->save();
        $msgReturn = ['status' => 'success', 'message' => 'URL shortened successfully!', 'short_url' => $shortURL, 'hits' => 0, 'expires_on' => $expires_on, 'long_url' => $long_url];
            return $msgReturn;    
    }

}