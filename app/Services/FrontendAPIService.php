<?php 

/**
 * API Services for FRONTEND Interface
 *
 * @author Sirish Shrestha
 */
namespace app\Services;
use Illuminate\Http\Request;
use App\User;
use App\ShortUrl;

class FrontendAPIService{
	protected $request;
	
	public function __construct(Request $request) {
        $this->request = $request;
    }

    /*
    Make a guzzle http request to the createShortURL API endpoint and 
    redirects to home with appropriate status and message.
    */
    public function CallCreateShortUrlAPIAndRedirect(){
    	$endpoint = url("/api/v1/createShortURL");
    	$user = User::find(1);
    	$api_token = $user->api_token;//USE API TOKEN saved in Database
    	$long_url = $this->request->input("long_url");
    	$expires_on = $this->request->input("expires_on");

		$client = new \GuzzleHttp\Client(['base_uri' => $endpoint]);
		$headers = [
		    'Authorization' => 'Bearer ' . $api_token,        
		    'Accept'        => 'application/json',
		];
		$requestParam = [
			'long_url' => $long_url,
			'expires_on' => $expires_on
		];
		$response = $client->request('POST', 'createShortURL', [
        'headers' => $headers, 'form_params' => $requestParam
   		])->getBody()->getContents();

    	$result = json_decode($response);

    	if($result->status == "validationError"){
    		return redirect('/home')
		    ->withInput()
		    ->withErrors($result->message);	
    	}else if($result->status == "error"){
    		return redirect('/home')
		    ->withInput()->with('statusError',$result->message);
    	}else{
    		return redirect('/home')
		    ->withInput()->with('status',$result->message)->with('short_url',$result->short_url)->with('long_url',$result->long_url)->with('hits',$result->hits)->with('expires_on',$result->expires_on);	
    	}
    }

    /*
    Checks if URL is deleted by Admin and redirects as 410
    Checks if URL link has expired and redirects to 404
    Checks if URL link is valid and redirects to long URL with 302 status
    */
    public function checkAndRedirectToLongUrl($url_hash){
    	$url = ShortUrl::where('url_hash', '=' ,$url_hash)->firstOrFail();
    	$today = date("Y-m-d");
		$expires_on = $url->expires_on;
		$today_dt = strtotime($today);
		$expire_dt = strtotime($expires_on);
		if($url->is_deleted == 1){
			return response("This URL has been deleted or blacklisted.", 410);
		}else if($url->expires_on !== null && $today_dt >= $expire_dt){
			return abort(404);
		}else{
			if($url->incrementHitCounter()->save()){
				return redirect()->away($url->long_url,'302');	
			}
		}
    }
}