<?php 
    /*
    GET ALPHANUMERIC COMBINATION IN SPECIFIC SEQUENCE
    */
    function get_next_alphanumeric($code){
        $chars = str_split("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        $code_array = str_split($code);
        //Starts a search for the next character that can be incremented, that is, unlike Z
        //Note that it starts from the last character to the first character
        for($i = count($code_array)-1;$i>-1;$i--){
        if($code_array[$i] == "Z"){
            if($i==0){
                //If it is equal to Z and is the first character, then length increases
                $code_array = array_fill(0,count($code_array) + 1,0);
                return implode("",$code_array);
            }else{
                if($code_array[$i -1] != 'Z'){
                    //If the previous character is different from Z, it increments it and clears the current and subsequent characters
                    //If the previous character is the first one, it also works because it increments it and zeroes the others.
                    $code_array[$i -1] = $chars[array_search($code_array[$i -1],$chars) + 1];
                    for($j = $i; $j < count($code_array); $j++){
                        $code_array[$j] = 0;
                    }
                    return implode("",$code_array);
                }
            }
        }else{
                //calculates the next character, that is, increments the current
                $code_array[$i] = $chars[array_search($code_array[$i],$chars) + 1];
                if($i == 0){
                    //If it is the first character, it means that the others are z
                    $novo_array = array_fill(0,count($code_array),0);
                    $novo_array[0] = $code_array[$i];
                    $code_array = $novo_array;
                }
                return implode("",$code_array);
        }
    }
}
