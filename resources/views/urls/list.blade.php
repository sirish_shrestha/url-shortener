@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Manage URLs</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>
                <form class="form-inline">
                        <div class="row">
                            <div class="col-lg-12">
                                <span class="styled-input">
                                    <input class="form-control input-sm" placeholder="Long URL Search"
                                           aria-controls="url-list" type="search" name="long_url_search"
                                           id="long_url_search">
                                </span>
                                <span class="styled-input">
                                    <input class="form-control input-sm" placeholder="Short URL Hash Search"
                                           aria-controls="url-list" type="search" name="short_url_search"
                                           id="short_url_search">
                                </span>
                            </div>
                        </div>
                </form>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-hover" id="url-list">
                            <thead>
                            <tr>
                                <th style="text-align:left;" width="2%">S.NO.</th>
                                <th style="text-align:left;" width="35%">Long URL</th>
                                <th style="text-align:left;" width="20%">Short URL</th>
                                <th style="text-align:left;" width="5%">Hits</th>
                                <th style="text-align:left;" width="15%">Expires On</th>
                                <th style="text-align:left;" width="15%">Actions</th>
                              </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="content-view-form" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Short URL Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3">Long URL</div>
          <div class="col-md-7 col-md-offset-2" id="long_url"></div>
        </div>
        <div class="row">
          <div class="col-md-3">Short URL</div>
          <div class="col-md-7 col-md-offset-2" id="short_url"></div>
        </div>
        <div class="row">
          <div class="col-md-3">Hits</div>
          <div class="col-md-7 col-md-offset-2" id="hits"></div>
        </div>
        <div class="row">
          <div class="col-md-3">Expires On</div>
          <div class="col-md-7 col-md-offset-2" id="expires"></div>
        </div>
        <div class="row">
          <div class="col-md-3">Created On</div>
          <div class="col-md-7 col-md-offset-2" id="created"></div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')
<script src="{{ url('js/components/typewatch/jquery.typewatch.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
        //create dynamic table via datatables.net plugin
        filtTable = $('#url-list').dataTable({
            lengthMenu: [25, 50, 100],//no of records per page options
            autoWidth: false,
            processing: true,
            serverSide: true,
            responsive: true,
            "searching": false,
            "dom": '<"top"f>rt<"bottom"ilp><"clear">',
            "ajax":{
                url :"{{ @url("/api/v1/urlsData") }}",
                type: "get",  // type of method  ,GET/POST/DELETE,
                "data": function(d){
                    d.long_url_search = $('#long_url_search').val();
                    d.short_url_search = $('#short_url_search').val();
                },
                beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + '{{$accessToken}}'); } //set tokenString before send
            },
            oLanguage: {
                "sInfo": "Page _PAGE_ of _PAGES_"
            },
            fnDrawCallback: function() {
                initGridBtns();
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr('rel', aData[5]); //add id to tr rel attribute
            },
            columns: [{sortable: false}, null, null, null, null, {sortable: false}, {visible: false}]
        });
        //Minimum 2 characters must be present to begin searching and wati for 500 ms before searching
        $('#long_url_search, #short_url_search').typeWatch({
                  wait: 500,
                  captureLength: 2,
                  callback: function(value) {
                    reloadDataTable();
                  }
                });
        $('#long_url_search, #short_url_search').keypress(function(e) {
                    if (e.keyCode == 13) {
                        e.preventDefault();
                    }
                });
    });
    function view_link(obj) {
        stack = [];
        stack.jq_context = $(obj);
        stack.item = stack.jq_context.parents('tr:eq(0)');
        stack.id = stack.item.attr('rel');
        $.ajax({
            url: "{{ url('api/v1/urls') }}/" + stack.id,
            type: 'GET',
            dataType: 'json',
            success: function(shorturl) {
                var viewModal = $('#content-view-form');
                $('#long_url', viewModal).html(shorturl.long_url);
                $('#short_url', viewModal).html("http://short.ly/"+shorturl.url_hash+"&nbsp;<a href='http://short.ly/"+shorturl.url_hash+"' target='_blank'><i class='fa fa-link'></i></a>");
                $('#hits', viewModal).html(shorturl.hits);
                $('#expires', viewModal).html(shorturl.expires_on);
                $('#created', viewModal).html(shorturl.created_at);
                $('#content-view-form').modal('show');
            },
            error: function() {
                alert('Oops! something went wrong.');
            },
            beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + '{{$accessToken}}'); } //set tokenString before send
        });
    }

    function delete_link(obj) {
        if(!confirm("Are you sure you want to delete this Shortened URL?")){
            return false;
        }
        stack = [];
        stack.jq_context = $(obj);
        stack.item = stack.jq_context.parents('tr:eq(0)');
        stack.id = stack.item.attr('rel');
        $.ajax({
            url: "{{ url('/api/v1/urls/') }}"+"/"+stack.id,
            type: 'DELETE',
            dataType: 'json',
            success: function(response_json) {
                if (response_json.status == true) {
                    alert('Short URL mapping deleted successfully!');
                    reloadDataTable();
                }else{
                    alert('Oops! something went wrong.');
                }
            },
            error: function() {
                alert('Oops! something went wrong.');
            },
            beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + '{{$accessToken}}'); } //set tokenString before send
        });
    }
    function initGridBtns() {
        //keep aside if no records on that dtable
        if ($('#url-list .dataTables_empty').length > 0) {
            return;
        }
        //Add View/Delete Links
        var viewLink = '<a onclick="view_link(this)" href="javascript:void(0)" title="View">View</a> ';
        var deleteLink = '&nbsp;&nbsp;<a onclick="delete_link(this)" href="javascript:void(0)" title="Delete">Delete</a> ';
        $('#url-list tbody tr td:last-child').html(viewLink + deleteLink);
    }
    reloadDataTable = function(){
        $('#url-list').DataTable().ajax.reload();
    }
</script>
@endsection