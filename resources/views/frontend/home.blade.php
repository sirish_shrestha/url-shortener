@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Make your URLs short</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            <br><br>
                            LONG URL: {{ session('long_url') }}<br>
                            SHORT URL: {{ session('short_url') }}<br>
                            Total Hits: {{ session('hits') }}<br>
                            Expires On: {{ session('expires_on') }}
                            
                        </div>
                        
                    @endif
                    
                    @if (session('statusError'))
                        <div class="alert alert-danger">
                            {{ session('statusError') }}
                        </div>
                    @endif
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="post" action="{{ url("/process-url") }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="usr">Type a link below to shorten it:</label>
                            <input type="text" class="form-control" id="long_url" name="long_url" placeholder="Long URL" value="{{old('long_url')}}">
                        </div>
                        <div class="form-group">
                            <label for="date">Expiry Date: (Optional) </label>
                            <input type="date" class="form-control" id="expires_on" name="expires_on" placeholder = "yyyy-mm-dd" value="{{old('expires_on')}}">
                        </div>
                        <input type="submit" class="btn btn-info" value="SHORTEN URL">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
