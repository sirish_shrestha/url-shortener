<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*FRONTEND ROUTES*/
Route::get('/home', 'FrontendController@index');
Route::post('/process-url', 'FrontendController@processURL');
//Main route to redirect the short URLs to respective long URLs
Route::get('/{url_hash}', 'FrontendController@redirect')->where('url_hash', '[A-Za-z0-9]{7}');


/*ADMIN ROUTES*/
Auth::routes();
//Dashboard Route
Route::get('/dashboard', 'HomeController@admin')->name('dashboard');
//Routes to manage short URLs
Route::get('/urls', 'URLController@index');
Route::get('/setUserAccessToken', 'HomeController@setUserAccessToken');



// Route  to request access token for Implicit Authorization Grant
Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '2',
        'redirect_uri' => 'http://short.ly/setUserAccessToken',
        'response_type' => 'token',
        'scope' => '',
    ]);

    return redirect('http://short.ly/oauth/authorize?'.$query);
});

