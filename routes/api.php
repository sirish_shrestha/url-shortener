<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function () {
	//API Routes
	Route::get('v1/urlsData', 'APIControllerV1@getUrlJSON');
	Route::get('v1/urls/{id}', 'APIControllerV1@getShortUrl');
	Route::delete('v1/urls/{id}', 'APIControllerV1@deleteShortUrl');
	Route::post('v1/createShortURL', 'APIControllerV1@createShortURL');
});