<?php

use Illuminate\Database\Seeder;

class ShortUrlsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('short_urls')->insert([
            'long_url' => 'http://short.ly',
            'url_hash' => '0000000',
            'is_deleted' => 0,
            'hits' => 0,
            'expires_on' => NULL,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
