---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://short.ly/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_4efb910b9c817d2b68a3a6fa2869bbf2 -->
## api/v1/urlsData

> Example request:

```bash
curl -X GET "http://short.ly/api/v1/urlsData" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://short.ly/api/v1/urlsData",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/v1/urlsData`

`HEAD api/v1/urlsData`


<!-- END_4efb910b9c817d2b68a3a6fa2869bbf2 -->

<!-- START_5c517dbd461db9c32b171dcb6c106405 -->
## api/v1/urls/{id}

> Example request:

```bash
curl -X GET "http://short.ly/api/v1/urls/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://short.ly/api/v1/urls/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/v1/urls/{id}`

`HEAD api/v1/urls/{id}`


<!-- END_5c517dbd461db9c32b171dcb6c106405 -->

<!-- START_122912bd6b0ade7cc8654d5a2df59337 -->
## api/v1/urls/{id}

> Example request:

```bash
curl -X DELETE "http://short.ly/api/v1/urls/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://short.ly/api/v1/urls/{id}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/v1/urls/{id}`


<!-- END_122912bd6b0ade7cc8654d5a2df59337 -->

<!-- START_34b68252e8e760d1b3cba6b366176e37 -->
## api/v1/createShortURL

> Example request:

```bash
curl -X POST "http://short.ly/api/v1/createShortURL" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://short.ly/api/v1/createShortURL",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/createShortURL`


<!-- END_34b68252e8e760d1b3cba6b366176e37 -->

