## About URL SHORTENER

URL Shortener is a simple PHP laravel application which creates a 7 character fixed length short alphanumeric URL from a given long URL.

## KEY FEATURES
- 7 alphanumeric character based Short URL generation. Supported characters are: 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
- URL can be specified with a expiry date.
- Total hits counter for URL
- Has Admin Interface with Short URLs list and ability to view/delete the generated URLs.
- Restful APIs

## SYSTEM REQUIREMENTS
- PHP >= 7.1.3
- PostGres:9.5.13
- Laravel 5.6

## LARAVEL PACKAGES USED
- PASSPORT
- GUZZLE

## INSTALLATION / SETUP

This installation guide is for Windows using Xampp.
```
1. Go to Xampp htdocs folder and git clone the repository: 
git clone https://<your_bitbucket_username>@bitbucket.org/sirish_shrestha/url-shortener.git

2. Set Up Virtual Hosts in the httpd-vhosts.conf file as below:
<VirtualHost *:80>
	ServerName short.ly
	ServerAlias www.short.ly
	DocumentRoot E:/xamppLatest/htdocs/url-shortener/public
	<Directory  "E:/xamppLatest/htdocs/url-shortener/public">
		Order allow,deny
        Allow from all
		Require all granted
	</Directory>
</VirtualHost>
Note: Make sure the path is correct in document root and directory above.

3. Add the below code in Window HOST file (i.e. C:\Windows\System32\drivers\etc\hosts):
127.0.0.1   short.ly

4. COPY ".env.example" file to ".env" in root folder of the laravel project.
Configure database settings as per your local settings.

5. Run command in the root project folder:
composer install

6. Run Migrations and Seeders:
- php artisan migrate
- php artisan db:seed

7. Generate Key:
php artisan key:generate

8. Generate oauth private and public key in storage folder for Passport
php artisan passport:keys

9. Generate a passport client and provide information as below:
php artisan passport:client
-Which user ID should the client be assigned to?:
>1
-What should we name the client?
> Your Name
-Where should we redirect the request after authorization?
>http://short.ly/setUserAccessToken

Note: COPY the client id generated in this process and use the same client id in the URL in the next step (i.e. Step 10)
 to generate the access Token.

10. Now Run the below URL to generate the access token for Implicit Authorization Grant for test purpose:
http://short.ly/oauth/authorize?client_id=<REPLACE_WITH_CLIENT_ID_GENERATED_INSTEP_9>&redirect_uri=http%3A%2F%2Fshort.ly%2FsetUserAccessToken&response_type=token&scope=

11. Click on "Authorize" and copy the access token in the URL and save it to **api_token** field in **users** table
 in the database for that specific user id.
```

## APPLICATION URL OVERVIEW

###FRONTEND URL
####Homepage Link:
http://short.ly/home
Input fields: 
- Long URL: Required
- Expiry Date: Optional
Validataions: URL validation and expiry date > current date validataion.

###BACKEND URL**
####Dashboard Link:
http://short.ly/dashboard
Default Username/Password: **admin@admin.com/secret**

####Manage URLs Link: 
http://short.ly/urls
Features: View, Delete, Search by Long URL, Short URL hash.

## API ENDPOINTS

###POST api/v1/createShortURL
####Submit a link to create a Short URL

#####EXAMPLE REQUEST FORMAT:
```
curl -X POST "http://short.ly/api/v1/createShortURL" \
-H "Accept: application/json" \
-H "Authorization: Bearer <YOUR-BEARER_TOKEN>" \
-d "long_url=http://www.pictures-of-nepal.com&expires_on=2019-08-02"
```

#####EXAMPLE RESPONSE FORMAT:
```
{"status":"success","message":"URL shortened successfully!","short_url":"http:\/\/short.ly\/0000006",
"hits":0,"expires_on":"2019-08-02","long_url":"http:\/\/www.pictures-of-nepal.com"}




```



###GET api/v1/urls/{id}
####GET SHORTENED URL by id

#####EXAMPLE REQUEST FORMAT:
```
curl -X GET "http://short.ly/api/v1/urls/9" \
-H "Accept: application/json" \
-H "Authorization: Bearer <YOUR-BEARER_TOKEN>"
```

#####EXAMPLE RESPONSE FORMAT:
```
{
    "id": 9,
    "long_url": "http://www.soundcloud9.com",
    "url_hash": "999999a",
    "is_deleted": 0,
    "hits": "0",
    "expires_on": "2018-10-20 00:00:00",
    "created_at": "2018-08-12 18:41:34",
    "updated_at": "2018-08-12 18:41:34"
}




```



###DELETE api/v1/urls/{id}
####DELETE SHORTENED URL by id

#####EXAMPLE REQUEST FORMAT:
```
curl -X DELETE "http://short.ly/api/v1/urls/9" \
-H "Accept: application/json" \
-H "Authorization: Bearer <YOUR-BEARER_TOKEN>"
```

#####EXAMPLE RESPONSE FORMAT:
```
{"status":true}




```


###GET api/v1/urlsData
####GET SHORT URLs List that is ready to be used for Data Tables in admin
#####Parameters
```
1. draw : Integer

This is used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in 
sequence by DataTables

2. start: Integer
Paging first record indicator. This is the start point in the current data set (0 index based - i.e. 0 is the 
 first record).

3. length: Integer
Number of records that the table can display in the current draw.

4. long_url_search: String
The Long Url text to search for.

5. short_url_search: String
The Short URL text to search for.

6. order[i][column]: Innteger
Column to which ordering should be applied.

7. order[i][dir]L: String
Ordering direction for this column. 

8. columns[i][data]: String
Column's data source

9.columns[i][name]: String
Column's name

10. columns[i][searchable]: boolean
Flag to indicate if this column is searchable

11. columns[i][orderable]: boolean
Flag to indicate if this column is orderable 
```

#####EXAMPLE REQUEST FORMAT:
```
curl -X GET "http://short.ly/api/v1/urlsData?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=25&search%5Bvalue%5D=&search%5Bregex%5D=false&long_url_search=&short_url_search=&_=1534216442182" \
-H "Accept: application/json" \
-H "Authorization: Bearer <YOUR-BEARER_TOKEN>""
```

#####EXAMPLE RESPONSE FORMAT:
```
{
    "draw": "1",
    "limit": "25",
    "recordsTotal": 12,
    "recordsFiltered": 12,
    "data": [
        [
            1,
            "http://www.showtimenepal.com/upcoming-movies.html",
            "http://short.ly/34734df&nbsp;<a href='http://short.ly/34734df' target='_blank'><i class='fa fa-link'></a></i>",
            "7",
            "Aug 18, 2018 12:00PM",
            2,
            ""
        ],
        [
            2,
            "http://www.soundcloud2.com",
            "http://short.ly/asdfaspp&nbsp;<a href='http://short.ly/asdfaspp' target='_blank'><i class='fa fa-link'></a></i>",
            "0",
            "Oct 20, 2018 12:00AM",
            7,
            ""
        ],
        [
            3,
            "http://www.picnepal.com",
            "http://short.ly/999999b&nbsp;<a href='http://short.ly/999999b' target='_blank'><i class='fa fa-link'></a></i>",
            "5",
            "Aug 14, 2018 12:00AM",
            10,
            ""
        ],
        [
            4,
            "http://www.facebook.com",
            "http://short.ly/999999c&nbsp;<a href='http://short.ly/999999c' target='_blank'><i class='fa fa-link'></a></i>",
            "0",
            "Aug 16, 2018 12:00AM",
            11,
            ""
        ]
    ]
}

```